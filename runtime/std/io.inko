#! Types for core IO functionality.
#!
#! The IO module provides the basic building blocks for IO operations such as
#! reading from and writing to a file.

import std::conversion::ToString
import std::error::StandardError
import std::os::NEWLINE
import std::process

## The object to use when throwing IO related errors.
##
## This is simply a re-exported `StandardError` so code using this type doesn't
## have to be changed if we ever decide to use a custom object for errors.
let Error = StandardError

## A trait providing a common way for disposing of objects.
trait Close {
  ## Diposes of any external resources that may be associated with `self`.
  ##
  ## Sending `close` multiple times to the same object is allowed and should not
  ## result in any errors being thrown or panics being triggered.
  def close -> Nil
}

## Trait for retrieving the size of an IO object.
trait Size {
  def size !! Error -> Integer
}

## Trait for performing read operations on an object.
trait Read {
  ## Reads all available data and returns it as a `String`.
  def read !! Error -> String

  ## Reads up to the given number of bytes, returning them as a `String`.
  ##
  ## The read `String` may be smaller than the given number of bytes.
  def read_exact(amount: Integer) !! Error -> String
}

## Trait for performing write operations on an object.
trait Write {
  ## Writes the given object to the output stream, returning the number of bytes
  ## that were written.
  def write(data: ToString) !! Error -> Integer

  ## Writes the given `String` and a trailing newline to the object.
  def print(data: ?ToString = Nil) !! Error -> Nil {
    process.blocking {
      try write(data.to_string + NEWLINE)
    }

    Nil
  }

  ## Flushes any pending writes.
  def flush !! Error -> Nil
}

## Trait for seeking to a given offset in a stream of bytes.
trait Seek {
  ## Seeks to the given byte offset, returning the new offset.
  def seek(position: Integer) !! Error -> Integer
}

## Trait for performing both read and write operations on an object.
trait ReadWrite: Read + Write {}
